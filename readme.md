## gitlab-runner with docker-compose and privileged containers

```shell
git clone https://gitlab.com/jenneron/gitlab-runner-docker-compose
cd gitlab-runner-docker-compose
cp .env.example .env # edit it
docker compose up 
```
